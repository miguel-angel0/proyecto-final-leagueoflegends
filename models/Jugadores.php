<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores".
 *
 * @property int $codigo_jugador
 * @property string|null $nombre_j
 * @property string|null $rol
 * @property string|null $nacionalidad
 * @property string|null $descripcion
 * @property string|null $nick
 * @property int|null $codigo_equipo
 *
 * @property Banean[] $baneans
 * @property Equipos $codigoEquipo
 * @property Pickean[] $pickeans
 */
class Jugadores extends \yii\db\ActiveRecord
{
    
     public $imagen;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion'], 'string'],
            [['codigo_equipo'], 'integer'],
            [['nombre_j'], 'string', 'max' => 25],
            [['rol', 'nacionalidad', 'nick'], 'string', 'max' => 30],
            [['codigo_equipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['codigo_equipo' => 'codigo_equipo']],
            [['imagen'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_jugador' => 'Codigo Jugador',
            'nombre_j' => 'Nombre',
            'rol' => 'Rol',
            'nacionalidad' => 'Nacionalidad',
            'descripcion' => 'Descripcion',
            'nick' => 'Nick',
            'codigo_equipo' => ' Equipo',
        ];
    }

    /**
     * Gets query for [[Baneans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBaneans()
    {
        return $this->hasMany(Banean::className(), ['codigo_jugador' => 'codigo_jugador']);
    }

    /**
     * Gets query for [[CodigoEquipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEquipo()
    {
        return $this->hasOne(Equipos::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[Pickeans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPickeans()
    {
        return $this->hasMany(Pickean::className(), ['codigo_jugador' => 'codigo_jugador']);
    }
}
