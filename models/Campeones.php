<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "campeones".
 *
 * @property int $codigo_campeon
 * @property string $nombre_champ
 *
 * @property Banean[] $baneans
 * @property Pickean[] $pickeans
 */
class Campeones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'campeones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_champ'], 'required'],
            [['nombre_champ'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_campeon' => 'Codigo Campeon',
            'nombre_champ' => 'Nombre campeón',
        ];
    }

    /**
     * Gets query for [[Baneans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBaneans()
    {
        return $this->hasMany(Banean::className(), ['codigo_campeon' => 'codigo_campeon']);
    }

    /**
     * Gets query for [[Pickeans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPickeans()
    {
        return $this->hasMany(Pickean::className(), ['codigo_campeon' => 'codigo_campeon']);
    }
}
