<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "patrocinadores".
 *
 * @property string $nombre_p
 * @property int $codigo_equipo
 *
 * @property Equipos $codigoEquipo
 */
class Patrocinadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'patrocinadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_p', 'codigo_equipo'], 'required'],
            [['codigo_equipo'], 'integer'],
            [['nombre_p'], 'string', 'max' => 150],
            [['nombre_p', 'codigo_equipo'], 'unique', 'targetAttribute' => ['nombre_p', 'codigo_equipo']],
            [['codigo_equipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['codigo_equipo' => 'codigo_equipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre_p' => 'Nombres',
            'codigo_equipo' => ' Equipo',
        ];
    }

    /**
     * Gets query for [[CodigoEquipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEquipo()
    {
        return $this->hasOne(Equipos::className(), ['codigo_equipo' => 'codigo_equipo']);
    }
}
