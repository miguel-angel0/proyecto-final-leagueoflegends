<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "banean".
 *
 * @property int $codigo_partido
 * @property int $codigo_campeon
 * @property int $codigo_jugador
 *
 * @property Campeones $codigoCampeon
 * @property Jugadores $codigoJugador
 * @property Partidos $codigoPartido
 */
class Banean extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banean';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_partido', 'codigo_campeon', 'codigo_jugador'], 'required'],
            [['codigo_partido', 'codigo_campeon', 'codigo_jugador'], 'integer'],
            [['codigo_partido', 'codigo_campeon', 'codigo_jugador'], 'unique', 'targetAttribute' => ['codigo_partido', 'codigo_campeon', 'codigo_jugador']],
            [['codigo_campeon'], 'exist', 'skipOnError' => true, 'targetClass' => Campeones::className(), 'targetAttribute' => ['codigo_campeon' => 'codigo_campeon']],
            [['codigo_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['codigo_jugador' => 'codigo_jugador']],
            [['codigo_partido'], 'exist', 'skipOnError' => true, 'targetClass' => Partidos::className(), 'targetAttribute' => ['codigo_partido' => 'codigo_partido']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_partido' => ' Partido',
            'codigo_campeon' => ' Campeon',
            'codigo_jugador' => ' Jugador',
        ];
    }

    /**
     * Gets query for [[CodigoCampeon]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCampeon()
    {
        return $this->hasOne(Campeones::className(), ['codigo_campeon' => 'codigo_campeon']);
    }

    /**
     * Gets query for [[CodigoJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugador()
    {
        return $this->hasOne(Jugadores::className(), ['codigo_jugador' => 'codigo_jugador']);
    }

    /**
     * Gets query for [[CodigoPartido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartido()
    {
        return $this->hasOne(Partidos::className(), ['codigo_partido' => 'codigo_partido']);
    }
}
