<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Entrenadores;

/**
 * EntrenadoresSearch represents the model behind the search form of `app\models\Entrenadores`.
 */
class EntrenadoresSearch extends Entrenadores
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_entrenador', 'edad', 'codigo_equipo'], 'integer'],
            [['descripcion', 'alias', 'nacionalidad', 'nombre_coach'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Entrenadores::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_entrenador' => $this->codigo_entrenador,
            'edad' => $this->edad,
            'codigo_equipo' => $this->codigo_equipo,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'nacionalidad', $this->nacionalidad])
            ->andFilterWhere(['like', 'nombre_coach', $this->nombre_coach]);

        return $dataProvider;
    }
}
