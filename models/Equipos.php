<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipos".
 *
 * @property int $codigo_equipo
 * @property string|null $descripcion
 * @property string|null $nombre_e
 *
 * @property Entrenadores[] $entrenadores
 * @property Juegan[] $juegans
 * @property Partidos[] $codigoPartidos
 * @property Jugadores[] $jugadores
 * @property Patrocinadores[] $patrocinadores
 */
class Equipos extends \yii\db\ActiveRecord
{
    
    public $imagen;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion'], 'string'],
            [['nombre_e'], 'string', 'max' => 25],
            [['imagen'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_equipo' => 'Codigo Equipo',
            'descripcion' => 'Descripcion',
            'nombre_e' => 'Nombre ',
        ];
    }

    /**
     * Gets query for [[Entrenadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenadores()
    {
        return $this->hasMany(Entrenadores::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[Juegans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegans()
    {
        return $this->hasMany(Juegan::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[CodigoPartidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartidos()
    {
        return $this->hasMany(Partidos::className(), ['codigo_partido' => 'codigo_partido'])->viaTable('juegan', ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[Jugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadores()
    {
        return $this->hasMany(Jugadores::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[Patrocinadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPatrocinadores()
    {
        return $this->hasMany(Patrocinadores::className(), ['codigo_equipo' => 'codigo_equipo']);
    }
}
