<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "juegan".
 *
 * @property int $codigo_partido
 * @property int $codigo_equipo
 * @property int|null $nº_asesinatos
 * @property int|null $barones_derrotados
 * @property int|null $dragones_obtenidos
 * @property int|null $inhibidores_destruidos
 * @property int|null $destruccion_nexo
 * @property int|null $torres_destruidas
 * @property int|null $oro_conseguido
 *
 * @property Equipos $codigoEquipo
 * @property Partidos $codigoPartido
 */
class Juegan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'juegan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_partido', 'codigo_equipo'], 'required'],
            [['codigo_partido', 'codigo_equipo', 'nº_asesinatos', 'barones_derrotados', 'dragones_obtenidos', 'inhibidores_destruidos', 'destruccion_nexo', 'torres_destruidas', 'oro_conseguido'], 'integer'],
            [['codigo_partido', 'codigo_equipo'], 'unique', 'targetAttribute' => ['codigo_partido', 'codigo_equipo']],
            [['codigo_equipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['codigo_equipo' => 'codigo_equipo']],
            [['codigo_partido'], 'exist', 'skipOnError' => true, 'targetClass' => Partidos::className(), 'targetAttribute' => ['codigo_partido' => 'codigo_partido']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_partido' => ' Partido',
            'codigo_equipo' => ' Equipo',
            'nº_asesinatos' => 'Nº Asesinatos',
            'barones_derrotados' => 'Barones Derrotados',
            'dragones_obtenidos' => 'Dragones Obtenidos',
            'inhibidores_destruidos' => 'Inhibidores Destruidos',
            'destruccion_nexo' => ' Resultado',
            'torres_destruidas' => 'Torres Destruidas',
            'oro_conseguido' => 'Oro Conseguido',
        ];
    }

    /**
     * Gets query for [[CodigoEquipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEquipo()
    {
        return $this->hasOne(Equipos::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[CodigoPartido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartido()
    {
        return $this->hasOne(Partidos::className(), ['codigo_partido' => 'codigo_partido']);
    }
}
