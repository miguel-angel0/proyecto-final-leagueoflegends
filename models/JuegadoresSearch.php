<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Jugadores;

/**
 * JuegadoresSearch represents the model behind the search form of `app\models\Jugadores`.
 */
class JuegadoresSearch extends Jugadores
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_jugador', 'codigo_equipo'], 'integer'],
            [['nombre_j', 'rol', 'nacionalidad', 'descripcion', 'nick'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Jugadores::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_jugador' => $this->codigo_jugador,
            'codigo_equipo' => $this->codigo_equipo,
        ]);

        $query->andFilterWhere(['like', 'nombre_j', $this->nombre_j])
            ->andFilterWhere(['like', 'rol', $this->rol])
            ->andFilterWhere(['like', 'nacionalidad', $this->nacionalidad])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'nick', $this->nick]);

        return $dataProvider;
    }
}
