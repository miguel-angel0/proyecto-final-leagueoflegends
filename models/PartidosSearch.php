<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Partidos;

/**
 * PartidosSearch represents the model behind the search form of `app\models\Partidos`.
 */
class PartidosSearch extends Partidos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_partido'], 'integer'],
            [['fase_jornada', 'fecha'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Partidos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_partido' => $this->codigo_partido,
            'fecha' => $this->fecha,
        ]);

        $query->andFilterWhere(['like', 'fase_jornada', $this->fase_jornada]);

        return $dataProvider;
    }
}
