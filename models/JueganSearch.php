<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Juegan;

/**
 * JueganSearch represents the model behind the search form of `app\models\Juegan`.
 */
class JueganSearch extends Juegan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_partido', 'codigo_equipo', 'nº_asesinatos', 'barones_derrotados', 'dragones_obtenidos', 'inhibidores_destruidos', 'destruccion_nexo', 'torres_destruidas', 'oro_conseguido'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Juegan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_partido' => $this->codigo_partido,
            'codigo_equipo' => $this->codigo_equipo,
            'nº_asesinatos' => $this->nº_asesinatos,
            'barones_derrotados' => $this->barones_derrotados,
            'dragones_obtenidos' => $this->dragones_obtenidos,
            'inhibidores_destruidos' => $this->inhibidores_destruidos,
            'destruccion_nexo' => $this->destruccion_nexo,
            'torres_destruidas' => $this->torres_destruidas,
            'oro_conseguido' => $this->oro_conseguido,
        ]);

        return $dataProvider;
    }
}
