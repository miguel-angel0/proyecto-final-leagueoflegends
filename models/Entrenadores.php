<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entrenadores".
 *
 * @property int $codigo_entrenador
 * @property string|null $descripcion
 * @property string|null $alias
 * @property string|null $nacionalidad
 * @property int|null $edad
 * @property int|null $codigo_equipo
 * @property string|null $nombre_coach
 *
 * @property Equipos $codigoEquipo
 */
class Entrenadores extends \yii\db\ActiveRecord
{
     public $imagen;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrenadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion'], 'string'],
            [['edad', 'codigo_equipo'], 'integer'],
            [['alias', 'nombre_coach'], 'string', 'max' => 25],
            [['nacionalidad'], 'string', 'max' => 30],
            [['codigo_equipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['codigo_equipo' => 'codigo_equipo']],
            [['imagen'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_entrenador' => 'Codigo Entrenador',
            'descripcion' => 'Descripcion',
            'alias' => 'Alias',
            'nacionalidad' => 'Nacionalidad',
            'edad' => 'Edad',
            'codigo_equipo' => ' Equipo',
            'nombre_coach' => 'Nombre ',
        ];
    }

    /**
     * Gets query for [[CodigoEquipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEquipo()
    {
        return $this->hasOne(Equipos::className(), ['codigo_equipo' => 'codigo_equipo']);
    }
}
