<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JuegadoresSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jugadores-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_jugador') ?>

    <?= $form->field($model, 'nombre_j') ?>

    <?= $form->field($model, 'rol') ?>

    <?= $form->field($model, 'nacionalidad') ?>

    <?= $form->field($model, 'descripcion') ?>

    <?php // echo $form->field($model, 'nick') ?>

    <?php // echo $form->field($model, 'codigo_equipo') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
