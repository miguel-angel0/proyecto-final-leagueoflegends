<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Jugadores */

$this->title = 'Crear Jugador';
$this->params['breadcrumbs'][] = ['label' => 'Jugadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jugadores-create">

    <h1>Crear jugadores</h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
