<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\JuegadoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jugadores';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jugadores-index" style="
    
    margin-top: 60px;
">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear jugador', ['create'], ['class' => 'button button5']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//            'codigo_jugador',
            [
                'attribute' => 'imagen',
                'format' => 'html',
                'label' => 'Imagen jugador',
                'value' => function ($model) {

                    return Html::img('../../web/jugadores/' . $model->nombre_j . '.png',
                                    ['width' => '150px']);
                },
            ],
            [
                'label' => 'Nombre',
                'attribute' => 'nombre_j',
            ],
            'nacionalidad',
            'nick',
            'rol',
            'descripcion:ntext',
            [
                'label' => 'Equipo',
                'attribute' => 'codigo_equipo',
                'value' => 'codigoEquipo.nombre_e',
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}  '],
        ],
    ]);
    ?>


</div>
