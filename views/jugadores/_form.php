<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$equipos = app\models\Equipos::find()->all();
$listData = ArrayHelper::map($equipos, 'codigo_equipo', 'nombre_e');
$rol=[  'Top', 'Jungler','Mid','Adc','Support'];

/* @var $this yii\web\View */
/* @var $model app\models\Jugadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jugadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre_j')->textInput(['maxlength' => true]) ?>

   <?= $form->field($model, 'rol')->dropDownList( ['Selecione rol','Top' => 'Top', 'Jungler' => 'Jungler', 'Mid' => 'Mid','Adc'=>'Adc','Support'=>'Support']); ?>
    

    <?= $form->field($model, 'nacionalidad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'nick')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_equipo')->dropDownList($listData, ['prompt' => 'Seleccione equipo']); ?>
     <?= $form->field($model, 'imagen')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'button button5'] ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
