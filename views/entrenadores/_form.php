<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$equipos = app\models\Equipos::find()->all();
$listData = ArrayHelper::map($equipos, 'codigo_equipo', 'nombre_e');

/* @var $this yii\web\View */
/* @var $model app\models\Entrenadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="entrenadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nacionalidad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'edad')->textInput() ?>

    <?= $form->field($model, 'codigo_equipo')->dropDownList($listData, ['prompt' => 'Seleccione equipo']); ?>

    <?= $form->field($model, 'nombre_coach')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'imagen')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'button button5']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
