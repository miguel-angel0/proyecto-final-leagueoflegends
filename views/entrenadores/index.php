<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EntrenadoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Entrenadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entrenadores-index"style="margin-bottom: 110px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Entrenador', ['create'], ['class' => 'button button5']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
             [
                'attribute' => 'imagen',
                'format' => 'html',
                'label' => 'Imagen entrenador',
                'value' => function ($model) {

                    return Html::img('../../web/entrenadores/' . $model->nombre_coach . '.png',
                                    ['width' => '150px']);
                },
            ], 
             [
                'label' => 'Nombre',
                'attribute' => 'nombre_coach',
                 ],
            'descripcion:ntext',
            'alias',
            'nacionalidad',
            'edad',
//            'codigo_equipo',
                       
            [
                'label' => 'Equipo',
                'attribute' => 'codigo_equipo',
                'value' => 'codigoEquipo.nombre_e',
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}  '],
        ],
    ]);
    ?>


</div>
