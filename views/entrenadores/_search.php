<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EntrenadoresSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="entrenadores-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_entrenador') ?>

    <?= $form->field($model, 'descripcion') ?>

    <?= $form->field($model, 'alias') ?>

    <?= $form->field($model, 'nacionalidad') ?>

    <?= $form->field($model, 'edad') ?>

    <?php // echo $form->field($model, 'codigo_equipo') ?>

    <?php // echo $form->field($model, 'nombre_coach') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
