<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entrenadores */

$this->title = 'Actualizar entrenador: ' . $model->codigo_entrenador;
//$this->params['breadcrumbs'][] = ['label' => 'Entrenadores', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->codigo_entrenador, 'url' => ['view', 'id' => $model->codigo_entrenador]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="entrenadores-update" style="
    margin-bottom: 60px;
    margin-top: 60px;
">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
