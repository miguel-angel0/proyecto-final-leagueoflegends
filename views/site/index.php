<?php

use rmrevin\yii\fontawesome\FA;
use kv4nt\owlcarousel\OwlCarouselWidget;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'LigaLOL';
?>
<html lang="es">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

<!--        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>

        <div class="container-fluid">
            <div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-top: 70px;">
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                </ol>

                <div class="carousel-inner">
                    <div class="item active">
                        <img src="../web/img/redbull.png" alt="RedBull" style="width:100%;">
                    </div>

                    <div class="item">
                        <img src="../web/img/banner.jpg" alt="Chicago" style="width:100%;">
                    </div>

                    <div class="item">
                        <img src="../web/img/banner2.jpg" alt="Chicago" style="width:100%;">
                    </div>

                    <div class="item">
                        <img src="../web/img/banner4.jpg" alt="Chicago" style="width:100%;">
                    </div>
                </div>

                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Anterior</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Siguiente</span>
                </a>
            </div>

        </div>
        <div class="container-home"style="margin-bottom: 30px;">
            <div class="container-carts" style="margin-bottom: 20px;">
                <div class="card">
                    <div class="face face1"style="padding-bottom: 40px;">
                        <div class="content">
                            <img src="../web/img/campeonhome.png" style="margin-left: 8px;">
                            <h3 style="
    margin-top: 10px;
    padding-top: 0px;
">Campeones</h3>
                        </div>
                    </div>
                    <div class="face face2" style="padding-bottom: 40px;">
                        <div class="content">
                            <?= Html::a('Administrar Campeones', ['/campeones/index']) ?>
                           <?= Html::a('Campeones más baneados', ['site/consulta1a']) ?>
                            <?= Html::a('Campeones más pickeados', ['site/consulta2a']) ?>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="face face1">
                        <div class="content">
                            <img src="../web/img/jugadores(1).png" style="margin-left: 8px;">
                            <h3>Jugadores</h3>
                        </div>
                    </div>
                    <div class="face face2">
                        <div class="content">
                            <?= Html::a('Administrar Jugadores  ', ['/jugadores/index']) ?>
                        

                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="face face1">
                        <div class="content">
                            <img src="../web/img/escudohome.png">
                            <h3>Equipos</h3>
                        </div>
                    </div>
                    <div class="face face2">
                        <div class="content">
                             <?= Html::a('Administrar Equipos  ', ['/equipos/index']) ?>
                             <?= Html::a('Clasificación  ', ['/site/consulta3a']) ?>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <div><p></p>
            <p></p>
            <p></p>
        </div>

        <div class="container-home">
            <div class="container-carts" style="top:-70px;margin-bottom: 35px;">
                <div class="card">
                    <div class="face face1">
                        <div class="content">
                            <img src="../web/img/patrocinadores.png" style="margin-left: 20px;">
                            <h3>Patrocinadores</h3>
                        </div>
                    </div>
                    <div class="face face2">
                        <div class="content">
                            <?= Html::a('Administrar Patrocinadores  ', ['/patrocinadores/index']) ?>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="face face1">
                        <div class="content">
                            <img src="../web/img/partidoshome.png" >
                            <h3>Partidos</h3>
                        </div>
                    </div>
                    <div class="face face2">
                        <div class="content">

                           <?= Html::a('Administrar Partidos  ', ['/partidos/index']) ?>
                            <?= Html::a('Administrar Picks  ', ['/pickean/index']) ?>
                            <?= Html::a('Administrar Bans  ', ['/banean/index']) ?>
                            <?= Html::a('Administrar Datos Partidos  ', ['/juegan/index']) ?>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="face face1">
                        <div class="content">
                            <img src="../web/img/entrenadorhome.png"style="margin-left: 8px;">
                            <h3>Entrenadores</h3>
                        </div>
                    </div>
                    <div class="face face2">
                        <div class="content">
                             <?= Html::a('Administrar Entrenadores  ', ['/entrenadores/index']) ?>
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>