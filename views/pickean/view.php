<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pickean */

$this->title = $model->codigo_partido;
$this->params['breadcrumbs'][] = ['label' => 'Pickeans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pickean-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigo_partido' => $model->codigo_partido, 'codigo_campeon' => $model->codigo_campeon, 'codigo_jugador' => $model->codigo_jugador], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigo_partido' => $model->codigo_partido, 'codigo_campeon' => $model->codigo_campeon, 'codigo_jugador' => $model->codigo_jugador], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_partido',
            'codigo_campeon',
            'codigo_jugador',
        ],
    ]) ?>

</div>
