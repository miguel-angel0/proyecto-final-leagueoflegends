<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pickean */

$this->title = 'Crear Pick';
$this->params['breadcrumbs'][] = ['label' => 'Picks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pickean-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
