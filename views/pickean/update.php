<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pickean */

$this->title = 'Actualizar Pick: ' ;
//$this->params['breadcrumbs'][] = ['label' => 'Pickeans', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->codigo_partido, 'url' => ['view', 'codigo_partido' => $model->codigo_partido, 'codigo_campeon' => $model->codigo_campeon, 'codigo_jugador' => $model->codigo_jugador]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pickean-update" style="
    margin-bottom: 60px;
    margin-top: 60px;
">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
