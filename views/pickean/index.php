<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BaneanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Picks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pickean-index"style="margin-bottom: 110px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear pick', ['create'], ['class' => 'button button5']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
        
            [ 
                'label'=> 'Partido',
                'attribute'=>'codigo_partido',
                'value'=>'codigoPartido.fecha', 
            ],
             [ 
                'label'=> 'Campeón',
                'attribute'=>'codigo_campeon',
                'value'=>'codigoCampeon.nombre_champ', 
            ],
               
           
            [
                'label'=> 'Jugador',
                'attribute'=>'codigo_jugador',
                'value'=>'codigoJugador.nick',
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}  '],
        ],
    ]); ?>


</div>
