<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Campeones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="campeones-form" style="
    margin-bottom: 60px;
">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre_champ')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'button button5']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
