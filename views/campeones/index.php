<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CampeonesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Campeones';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campeones-index" style="
   
    margin-top: 60px;
">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear campeón', ['/campeones/create'], ['class'=>'button button5']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//
//            'codigo_campeon',
             [
                'label' => 'Nombre del campeón',
                'attribute' => 'nombre_champ',
                 ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}  '],
        ],
    ]); ?>


</div>
