<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Campeones */

$this->title = 'Actualizar Campeón: ' ;
////$this->params['breadcrumbs'][] = ['label' => 'Campeones', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->codigo_campeon, 'url' => ['view', 'id' => $model->codigo_campeon]];
//$this->params['breadcrumbs'][] = 'Actualizar';
//?>
<div class="campeones-update" style="
    margin-bottom: 60px;
    margin-top: 60px;
">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
