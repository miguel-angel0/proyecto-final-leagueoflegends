<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EquiposSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Equipos';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="equipos-index"style="margin-bottom: 110px;
    
    margin-top: 60px;
">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Equipo', ['create'], ['class' => 'button button5']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'imagen',
                'format' => 'html',
                'label' => 'Escudo',
                'value' => function ($model) {

                    return Html::img('../../web/equipos/' . $model->nombre_e . '.png',
                                    ['width' => '100px']);
                },
            ], 
                        [
                'label' => 'Equipo',
                'attribute' => 'nombre_e', 
            ],
//            'nombre_e',
            'descripcion:ntext',
//           
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}  '],
        ],
    ]);
    ?>


</div>
