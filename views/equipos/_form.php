<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$equipos = app\models\Equipos::find()->all();
$listData = ArrayHelper::map($equipos, 'codigo_equipo', 'nombre_e');

/* @var $this yii\web\View */
/* @var $model app\models\Equipos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="equipos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'nombre_e')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'imagen')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'button button5']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
