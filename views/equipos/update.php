<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Equipos */

$this->title = 'Actualizar Equipo: ' ;
//$this->params['breadcrumbs'][] = ['label' => 'Equipos', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->codigo_equipo, 'url' => ['view', 'id' => $model->codigo_equipo]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="equipos-update" style="
    margin-bottom: 60px;
    margin-top: 60px;
">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
