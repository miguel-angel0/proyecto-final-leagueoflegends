<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JueganSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="juegan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_partido') ?>

    <?= $form->field($model, 'codigo_equipo') ?>

    <?= $form->field($model, 'nº_asesinatos') ?>

    <?= $form->field($model, 'barones_derrotados') ?>

    <?= $form->field($model, 'dragones_obtenidos') ?>

    <?php // echo $form->field($model, 'inhibidores_destruidos') ?>

    <?php // echo $form->field($model, 'destruccion_nexo') ?>

    <?php // echo $form->field($model, 'torres_destruidas') ?>

    <?php // echo $form->field($model, 'oro_conseguido') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
