<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\JueganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Resultados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="juegan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Resultado', ['create'], ['class' => 'button button5']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            [ 
                'label'=> 'Partido',
                'attribute'=>'codigo_partido',
                'value'=>'codigoPartido.fecha', 
            ],
            [
                'label' => 'Equipo',
                'attribute' => 'codigo_equipo',
                'value' => 'codigoEquipo.nombre_e',
            ],
            [
                'label' => 'Asesinatos',
                'attribute' => 'nº_asesinatos',
                
            ],
            [
                'label' => 'Barones',
                'attribute' => 'barones_derrotados',
                
            ],
            
            [
                'label' => 'Dragones',
                'attribute' => 'dragones_obtenidos',
                
            ],
            [
                'label' => 'Inhibidores',
                'attribute' => 'inhibidores_destruidos',
                
            ],
            
            [
                'label' => 'Torres',
                'attribute' => 'torres_destruidas',
                
            ],
            [
                'label' => 'Oro',
                'attribute' => 'oro_conseguido',
                
            ],
            [
                'label'=>'Resultado',
            'attribute' => 'destruccion_nexo',
            'value' => function($model){
                         if($model->destruccion_nexo == '1'){
                            return 'Victoria ';
                         }
                         else return 'Derrota';
                       }
],
            

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}  '],
        ],
    ]); ?>


</div>
