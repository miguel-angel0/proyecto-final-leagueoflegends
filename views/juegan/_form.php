<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$partidos = app\models\Partidos::find()->all();
$listData = ArrayHelper::map($partidos, 'codigo_partido', 'fecha');
$equipos = app\models\Equipos::find()->all();
$listData1 = ArrayHelper::map($equipos, 'codigo_equipo', 'nombre_e');
$destruccion_nexo=[ 'Victoria','Derrota'];

/* @var $this yii\web\View */
/* @var $model app\models\Juegan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="juegan-form" style="
    margin-bottom: 60px;
">

    <?php $form = ActiveForm::begin(); ?>

     <?= $form->field($model, 'codigo_partido')->dropDownList($listData, ['prompt' => 'Seleccione partido']); ?>

    <?= $form->field($model, 'codigo_equipo')->dropDownList($listData1, ['prompt' => 'Seleccione equipo']); ?>

    <?= $form->field($model, 'nº_asesinatos')->textInput() ?>

    <?= $form->field($model, 'barones_derrotados')->textInput() ?>

    <?= $form->field($model, 'dragones_obtenidos')->textInput() ?>

    <?= $form->field($model, 'inhibidores_destruidos')->textInput() ?>

    <?= $form->field($model, 'destruccion_nexo')->dropDownList( ['Selecione resultado: ','1' => 'Victoria', '0' => 'Derrota']); ?>

    <?= $form->field($model, 'torres_destruidas')->textInput() ?>

    <?= $form->field($model, 'oro_conseguido')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'button button5']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
