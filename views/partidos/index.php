<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PartidosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Partidos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partidos-index" style="margin-bottom: 110px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Partido', ['create'], ['class' => 'button button5']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'codigo_partido',
            'fase_jornada',
            'fecha',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}  '],
        ],
    ]); ?>


</div>
