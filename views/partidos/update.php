<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Partidos */

$this->title = 'Actualizar Partido: ' ;
//$this->params['breadcrumbs'][] = ['label' => 'Partidos', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->codigo_partido, 'url' => ['view', 'id' => $model->codigo_partido]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="partidos-update" style="
    margin-bottom: 60px;
    margin-top: 60px;
">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
