<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\Module;
use kartik\datecontrol\DateControl;
use yii\bootstrap4\Modal;
use kartik\datetime\DateTimePicker;




/* @var $this yii\web\View */
/* @var $model app\models\Partidos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partidos-form" style="
    margin-bottom: 60px;
">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model,'fase_jornada')->textInput()?>
    


    <?php echo $form->field($model, 'fecha')->widget(DateControl::classname(), [
        
    'type' => 'datetime',
    'ajaxConversion' => true,
    'autoWidget' => true,
    'widgetClass' => '',
    'displayFormat' => 'dd-MM-yyyy HH:mm A',
    'saveFormat' => 'php:Y-m-d H:i',
    'saveTimezone' => 'UTC',
        
    'displayTimezone' => 'UTC',
//    'saveOptions' => [
//        'label' => 'Input saved as: ',
//        'type' => 'text',
//        'readonly' => true,
//        'class' => 'hint-input text-muted'
//    ],
    'widgetOptions' => [
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd-MM-yyyy HH:mm A'
        ]
    ],
    'language' => 'es'
]);?>


    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'button button5']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
