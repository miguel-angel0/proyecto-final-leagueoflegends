<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$partidos = app\models\Partidos::find()->all();
$listData = ArrayHelper::map($partidos, 'codigo_partido', 'fecha');
$campeones = app\models\Campeones::find()->all();
$listData2 = ArrayHelper::map($campeones, 'codigo_campeon', 'nombre_champ');
$jugadores = app\models\Jugadores::find()->all();
$listData3 = ArrayHelper::map($jugadores, 'codigo_jugador', 'nick');


/* @var $this yii\web\View */
/* @var $model app\models\Banean */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banean-form"style="
    margin-bottom: 110px;
">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'codigo_partido')->dropDownList($listData, ['prompt' => 'Seleccione partido']); ?>

    <?= $form->field($model, 'codigo_campeon')->dropDownList($listData2, ['prompt' => 'Seleccione campeón']); ?>

    <?= $form->field($model, 'codigo_jugador')->dropDownList($listData3, ['prompt' => 'Seleccione jugador']); ?>


    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'button button5']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
