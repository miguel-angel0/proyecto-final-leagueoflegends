<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PatrocinadoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Patrocinadores';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patrocinadores-index" style="
    margin-bottom: 95px;
    margin-top: 60px;
">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Patrocinador', ['create'], ['class' => 'button button5']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Patrocinadores',
                'attribute' => 'nombre_p',
            ],
            [
                'label' => 'Equipo',
                'attribute' => 'codigo_equipo',
                'value' => 'codigoEquipo.nombre_e',
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}  '],
        ],
    ]); ?>


</div>
