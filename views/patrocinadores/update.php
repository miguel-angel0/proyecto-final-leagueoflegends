<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Patrocinadores */

$this->title = 'Actualizar Patrocinador: ' ;
//$this->params['breadcrumbs'][] = ['label' => 'Patrocinadores', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->nombre_p, 'url' => ['view', 'nombre_p' => $model->nombre_p, 'codigo_equipo' => $model->codigo_equipo]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="patrocinadores-update" style="
    margin-bottom: 60px;
    margin-top: 60px;
">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
