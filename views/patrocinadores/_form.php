<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$equipos= app\models\Equipos::find()->all();
$listData=ArrayHelper::map($equipos,'codigo_equipo','nombre_e');

/* @var $this yii\web\View */
/* @var $model app\models\Patrocinadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="patrocinadores-form" style="
    margin-bottom: 60px;
">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre_p')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_equipo')->dropDownList($listData, ['prompt' => 'Seleccione Equipo' ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'button button5']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
