<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use rmrevin\yii\fontawesome\FA;
use yii\bootstap\Dropdown;

AppAsset::register($this);
rmrevin\yii\fontawesome\AssetBundle::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/img/lvp.ico" type="image/x-icon" />



        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>



        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => Html::img('@web/img/lvp.png'),
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                    'id' => 'ColorH',
                ],
            ]);
            
            
             
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                   ['label' => 'Campeones', 'url' => ['/campeones/index']],
                    ['label' => 'Jugadores', 'url' => ['/jugadores/index']],
                    ['label' => 'Equipos', 'url' => ['/equipos/index']],
                   ['label' => 'Patrocinadores', 'url' => ['/patrocinadores/index']], 
                    ['label' => 'Partidos', 'url' => ['/partidos/index']],
                    
                    ['label' => 'Entrenadores', 'url' => ['/entrenadores/index']],
                   
                    
//            Yii::$app->user->isGuest ? (
//                ['label' => 'Login', 'url' => ['/site/login']]
//            ) : (
//                '<li>'
//                . Html::beginForm(['/site/logout'], 'post')
//                . Html::submitButton(
//                    'Logout (' . Yii::$app->user->identity->username . ')',
//                    ['class' => 'btn btn-link logout']
//                )
//                . Html::endForm()
//                . '</li>'
//            )
                ],
            ]);
            NavBar::end();
            ?>

            <div class="container">
                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>

        <footer>
            <div class="footerdark">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 col-md-3 item" style="left: 120px;">
                                <?= Html::img('@web/img/lvp.png', ['id' => 'logofooter']) ?>
                            </div>  
                          
                            <div class="col-sm-6 col-md-3"style="left: 200px;">
                                <h3>Navegación</h3>
                                <ul>
                                    <li> <?= Html::a(' Campeones  ', ['/campeones/index']) ?> </li>
                                    <li> <?= Html::a(' Jugadores  ', ['/Jugadores/index']) ?> </li>
                                    <li> <?= Html::a(' Equipos  ', ['/equipos/index']) ?> </li> 
                                    <li> <?= Html::a(' Patrocinadores  ', ['/patrocinadores/index']) ?> </li>
                                   <li> <?= Html::a(' Partidos  ', ['/partidos/index']) ?> </li><li> <?= Html::a(' Entrenadores  ', ['/entrenadores/index']) ?> </li>
                                    
                                   
                                   
                                </ul>
                            </div>

                          
                            <div id="imagenchamp" class="col-md-6 item">
                                <?= Html::img('@web/img/draven.png', ['id' => 'campeonfooter']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="copyright">    
                            <p>Tremenda Liga de las Leyendas © 2021</p>
                        </div>    
                    </div>

            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
