<?php

namespace app\controllers;

use Yii;
use app\models\Banean;
use app\models\BaneanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BaneanController implements the CRUD actions for Banean model.
 */
class BaneanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Banean models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BaneanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Banean model.
     * @param integer $codigo_partido
     * @param integer $codigo_campeon
     * @param integer $codigo_jugador
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_partido, $codigo_campeon, $codigo_jugador)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_partido, $codigo_campeon, $codigo_jugador),
        ]);
    }

    /**
     * Creates a new Banean model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Banean();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'codigo_partido' => $model->codigo_partido, 'codigo_campeon' => $model->codigo_campeon, 'codigo_jugador' => $model->codigo_jugador]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Banean model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $codigo_partido
     * @param integer $codigo_campeon
     * @param integer $codigo_jugador
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_partido, $codigo_campeon, $codigo_jugador)
    {
        $model = $this->findModel($codigo_partido, $codigo_campeon, $codigo_jugador);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'codigo_partido' => $model->codigo_partido, 'codigo_campeon' => $model->codigo_campeon, 'codigo_jugador' => $model->codigo_jugador]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Banean model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $codigo_partido
     * @param integer $codigo_campeon
     * @param integer $codigo_jugador
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_partido, $codigo_campeon, $codigo_jugador)
    {
        $this->findModel($codigo_partido, $codigo_campeon, $codigo_jugador)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Banean model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $codigo_partido
     * @param integer $codigo_campeon
     * @param integer $codigo_jugador
     * @return Banean the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_partido, $codigo_campeon, $codigo_jugador)
    {
        if (($model = Banean::findOne(['codigo_partido' => $codigo_partido, 'codigo_campeon' => $codigo_campeon, 'codigo_jugador' => $codigo_jugador])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
