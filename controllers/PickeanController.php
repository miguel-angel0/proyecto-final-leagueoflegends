<?php

namespace app\controllers;

use Yii;
use app\models\Pickean;
use app\models\PickeanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PickeanController implements the CRUD actions for Pickean model.
 */
class PickeanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pickean models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PickeanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pickean model.
     * @param integer $codigo_partido
     * @param integer $codigo_campeon
     * @param integer $codigo_jugador
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_partido, $codigo_campeon, $codigo_jugador)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_partido, $codigo_campeon, $codigo_jugador),
        ]);
    }

    /**
     * Creates a new Pickean model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pickean();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'codigo_partido' => $model->codigo_partido, 'codigo_campeon' => $model->codigo_campeon, 'codigo_jugador' => $model->codigo_jugador]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Pickean model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $codigo_partido
     * @param integer $codigo_campeon
     * @param integer $codigo_jugador
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_partido, $codigo_campeon, $codigo_jugador)
    {
        $model = $this->findModel($codigo_partido, $codigo_campeon, $codigo_jugador);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'codigo_partido' => $model->codigo_partido, 'codigo_campeon' => $model->codigo_campeon, 'codigo_jugador' => $model->codigo_jugador]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Pickean model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $codigo_partido
     * @param integer $codigo_campeon
     * @param integer $codigo_jugador
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_partido, $codigo_campeon, $codigo_jugador)
    {
        $this->findModel($codigo_partido, $codigo_campeon, $codigo_jugador)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pickean model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $codigo_partido
     * @param integer $codigo_campeon
     * @param integer $codigo_jugador
     * @return Pickean the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_partido, $codigo_campeon, $codigo_jugador)
    {
        if (($model = Pickean::findOne(['codigo_partido' => $codigo_partido, 'codigo_campeon' => $codigo_campeon, 'codigo_jugador' => $codigo_jugador])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
