<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\conditions\BetweenCondition;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1a() {

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT nombre_champ as campeón , COUNT(*)as NºBaneos FROM campeones c INNER JOIN banean b ON c.codigo_campeon = b.codigo_campeon GROUP BY c.nombre_champ ORDER BY NºBaneos DESC  ',
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['campeón', 'NºBaneos'],
                    "titulo" => "Número de veces baneado cada campeón",
                    "sql" => "SELECT nombre_champ , COUNT(*) as NºBaneos FROM campeones c INNER JOIN banean b ON c.codigo_campeon = b.codigo_campeon GROUP BY c.nombre_champ ORDER BY NºBaneos DESC  ",
        ]);
    }
    
     public function actionConsulta2a() {

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT nombre_champ as campeón , COUNT(*)as NºPickeos FROM campeones c INNER JOIN pickean p ON c.codigo_campeon = p.codigo_campeon GROUP BY c.nombre_champ ORDER BY NºPickeos DESC  ',
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['campeón', 'NºPickeos'],
                    "titulo" => "Número de veces pickeado cada campeón",
                    "sql" => "SELECT nombre_champ as campeón , COUNT(*)as NºPickeos FROM campeones c INNER JOIN pickean p ON c.codigo_campeon = p.codigo_campeon GROUP BY c.nombre_champ ORDER BY NºPickeos DESC",
        ]);
    }
    
     public function actionConsulta3a() {

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT nombre_e Equipo, COUNT(*) Victorias FROM equipos e INNER JOIN juegan j ON e.codigo_equipo = j.codigo_equipo WHERE j.destruccion_nexo=1 GROUP BY e.codigo_equipo ORDER BY Victorias DESC ',
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['Equipo', 'Victorias'],
                    "titulo" => "Clasificación",
                    "sql" => "SELECT nombre_e Equipo, COUNT(*) Victorias FROM equipos e INNER JOIN juegan j ON e.codigo_equipo = j.codigo_equipo WHERE j.destruccion_nexo=1 GROUP BY e.codigo_equipo ORDER BY Victorias DESC",
        ]);
    }
   
}
